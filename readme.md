# Shortest path in 2d space
How to find shortest path without using [A*](https://en.wikipedia.org/wiki/A*_search_algorithm)?

(
  because continuos space?
I can be wrong: 
If A* [can be generalized to continuous space](https://blog.habrador.com/2015/11/explaining-hybrid-star-pathfinding.html)
)

Hint: Let's use derivatives and gradients!

Bad news: the initial angle of the trajectory can be from 0 to 360 degrees. (Infinite amount of possibilities)

Fun fact:
__OptiMal__ path in 2d space with density function `n(x, y)`
is also __OptiCal__ path of light where `n(x, y)` is [refractive index](https://en.wikipedia.org/wiki/Refractive_index), two cases in one (stupid thing but funny)

### Current state of development:
* Path can be visualised only for given `startPoint` and `startAngle` (or `startTangent`).

# How to save mouse position (you can contribute to make this better)
* In browser: `F12` to run Developer Tools
* Type in console window `mouseHistory[mouseHistory.length - 1]` and hit Enter
* Right click and select `copy: object`

Example:
```js
{
    "targetPoint": {
        "x": 2.24,
        "y": -1.49
    },
    "startPoint": {
        "x": -0.77,
        "y": -2.59
    }
}
```

# How to find mathematically right (really shortest) continuous path?
Note: the possible number of paths is infinite but can be "squashed" from N-dimentional space to 1-d

Remember: the initial angle of the trajectory can be from 0 to 360 degrees. It's a circumference.
## Method 1
first method: exact equation

Use a bit of [variational calculus](https://en.wikipedia.org/wiki/Calculus_of_variations) to make this formulas
```js
let pseudoScalarProduct = (normdx * gy - normdy * gx);
let k = pseudoScalarProduct / n(x, y);
```
```js
let n_value = n(x, y);
let [gradlnx, gradlny] = [gx / n_value, gy / n_value];
// let [gradlnx, gradlny] = [normdy, -normdx];
// grad(ln(n)) * ds = k, where '*' <=> pseudoScalarProduct
```

## Method 2
second method: photon trajectory likeness

Just interate gravity force for particle of light.

Where `n(x, y)` is related to [gravitational potential](https://en.wikipedia.org/wiki/Gravitational_potential)

# Good formulation of problem
I don't really understand how to implement this but it's a interesting for reading.
https://www.researchgate.net/publication/37594759_Efficient_algorithms_for_continuous-space_shortest_path_problems

# Links from google
* path for robot 
  + https://stackoverflow.com/questions/37242916/continuous-space-shortest-path

* Eusclidean shortest path
  + https://en.wikipedia.org/wiki/Euclidean_shortest_path

* Continuous Path Planning with Multiple Constraints
  + https://www.cs.ubc.ca/~mitchell/Papers/cdcMCPP.pdf
