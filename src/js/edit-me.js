const funcList = {
  func1: {
    pixelColorMultiplier: 256 / 5,
    pixelColorOffset: 0,
    startPoint: { x: -1, y: -1 },
    targetPoint: {x: 0.82, y: - 0.86},
    n: (x, y) => {
    if (square(x, y, 0, 0, 1)) {
      return 1 + Math.sin(x) * 2;
    }
    if (square(x, y, 0, -1, 1)) {
      return 1 - x * 0.9;
    }
    return 1;
  }
},
  funcExp: {
    pixelColorMultiplier: 256 / 50,
    pixelColorOffset: 0,
    startPoint: { x: -0.8, y: -2.49 },
    targetPoint: { x: 2.45, y: -1.61 },
    n: (x, y) => {
      let r1 = (x) ** 2 + (y - 1) ** 2;
      let r2 = (x) ** 2 + (y + 1) ** 2;
      return 1 + 45 * (Math.exp(-5 * r1) + Math.exp(-5 * r2));
    }
  }
}

const currentDensityFunctionSetup = funcList.funcExp;

const scene = {
  startPoint: currentDensityFunctionSetup.startPoint,
  targetPoint: currentDensityFunctionSetup.targetPoint,
  finishPoint: { x: 0, y: 1 },
  pathOptions: {
    length: 10,
    step: 1 / PATH_STEP_INV,
    stepInversed: PATH_STEP_INV,
    drawLineEvery: ~~(PATH_STEP_INV / 20),
    leftRightPathsEnabled: true,
  },
  circle: {
    radius: {
      green: 2,
      blue: 1,
    },
  },
  pixelColorMultiplier: currentDensityFunctionSetup.pixelColorMultiplier,
  pixelColorOffset: currentDensityFunctionSetup.pixelColorOffset,
  width: SCENE_WIDTH,
  height: SCENE_HEIGHT,
};