const cv = document.getElementsByTagName('canvas')[0];
const SCENE_WIDTH = 800;
const SCENE_HEIGHT = 800;
cv.width = SCENE_WIDTH;
cv.height = SCENE_HEIGHT;
const ctx = cv.getContext('2d');
ctx.imageSmoothingEnabled = false;
// Update imageData object only at first calculation
let cachedImageData = null;
let mouseHistory = [];
let fromFiguresData = () => {
  throw new Error("Figures data not loaded");
}
let imageLoader = new ImageLoader({ filename: "figures.png" });

function setup() {
  const loadFigureEnabled = false;
  // setTargetPoint({x: 1.37, y: -1.795});
  if (loadFigureEnabled) {
    imageLoader.load(() => {
      setTargetPoint(scene.targetPoint);
    });
  } else {
    setTargetPoint(scene.targetPoint);
  }
}

// Position of screen
let offset = { x: SCENE_WIDTH / 2, y: SCENE_HEIGHT / 2 };
let multiplier = 100;

// Graphics constansts
const CIRCLE_RADIUS = 3;
const LINE_WIDTH = 1;

// Constants related to path calculation
const maxIterations = 10000;
const PATH_STEP_INV = 400; // 400

let dtanAlpha = 0.04;
const FLOAT_ERR = 0.0000000001;
let [mouseX, mouseY] = [0, 0];

function rect(x, y, rx, ry, rw, rh) {
  return x > rx && y > ry && x < rx + rw && y < ry + rh;
}

function square(x, y, sx, sy, size) {
  return rect(x, y, sx, sy, size, size);
}

function n(x, y) {
  // return 1.0 / sqrt(x*x + y*y); // test circle
  // return 1 + 0.8 * sin(y + x*x);
  // return 5 - (x*x + y*y); // light in black hole
  // return 1 - x*x; // sine wave ?
  // piecewise linear bump
  // return x > 1 && x < 2 ? 1 - 0.5 * (1 - 2 * abs(x - 1.5)) : 1;
  // let r1 = x**2 + y**2;
  // let r2 = (x - 1)**2 + y**2;
  // return 1 + 5*exp(-5 * r1) - 5*exp(-5 * r2);
  // return 1 + 0.5*sin(3*x)*sin(50*y);

  return currentDensityFunctionSetup.n(x, y);

  // return 3 * fromFiguresData(x, y) + 1;
}

function setTargetPoint(mpos) {
  let tanAlpha = 1;
  let dx = mpos.x - scene.startPoint.x;
  let dy = mpos.y - scene.startPoint.y;
  if (dx !== 0) {
    tanAlpha = dy / dx;
  }
  // Save angle
  scene.targetPoint = mpos;
  mouseHistory.push({targetPoint: mpos, startPoint: scene.startPoint});

  drawScene();
  calculateScene(scene.startPoint, scene.finishPoint, tanAlpha, dtanAlpha);
}

window.addEventListener('load', setup);