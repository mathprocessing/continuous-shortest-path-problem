function assert(value, message) {
  if (!value) {
    console.assert(value, message);
    throw new Error("Assertion error");
  }
}

const Vector = {
  /**
   * @param {{x: number, y: number}} v1 
   * @param {{x: number, y: number}} v2 
   * @returns {{x: number, y: number}}
   */
  add: (v1, v2) => {
    return { x: v1.x + v2.x, y: v1.y + v2.y };
  },

  /**
   * @param {{x: number, y: number}} v1 
   * @param {{x: number, y: number}} v2 
   * @returns {{x: number, y: number}}
   */
  sub: (v1, v2) => {
    return { x: v1.x - v2.x, y: v1.y - v2.y };
  }
};

function getMousePos() {
  let x = (mouseX - offset.x) / multiplier;
  let y = -(mouseY - offset.y) / multiplier;
  return { x, y };
}

/**
 * @param {number} x
 * @param {number} y
 * @param {number} targetLength 
 * @returns {[number, number]} 2d vector `[x, y]` with `length = targetLength`
 */
 function normalizeVector(x, y, targetLength = 1) {
  const coef = targetLength / Math.hypot(x, y);
	return [x * coef, y * coef];
}

/**
 * Calculate gradient of function `f(x, y)` in point `[x, y]`
 * @param {(x: number, y: number) => number} f input 2-argument function
 * @param {number} x 
 * @param {number} y 
 * @param {number} h discrete step
 * @returns {[number, number]} 2d vector `[x, y]`
 */
function grad(f, x, y, h) {
  const componentX = (f(x + h, y) - f(x - h, y)) / (2 * h);
  const componentY = (f(x, y + h) - f(x, y - h)) / (2 * h);
  return [componentX, componentY];
}

/**
 * Change from `[0 .. 1]` to `[0 .. SCENE_WIDTH]`
 * @param {[number, number]} p Point
 * @param {number} multiplier scale coefficient
 * @param {{x: number, y: number}} offset translation 2d vector
 * @returns {[number, number]} Point
 */
function changeCoordSystem(p, multiplier, offset) {
  return [p[0] * multiplier + offset.x, offset.y - p[1] * multiplier];
}

/**
 * Change from `[0 .. SCENE_WIDTH]` to `[0 .. 1]`
 * @param {[number, number]} p Point
 * @param {number} multiplier 
 * @param {{x: number, y: number}} offset translation 2d vector
 * @returns {[number, number]} Point
 */
function changeCoordSystemBack(p, multiplier, offset) {
  return [(p[0] - offset.x) / multiplier, (offset.y - p[1]) / multiplier];
}

/**
 * Change coordinate system of path object
 * @param {[[number, number]]} path Array of points
 * @param {number} multiplier scale coefficient
 * @param {{x: number, y: number}} offset translate 2d vector
 * @returns Array of points
 */
function changePathCoordSystem(path, multiplier, offset) {
  return path.map(p => changeCoordSystem(p, multiplier, offset));
}

/**
 * Show point as text
 * @param {[number, number]} p Point
 * @returns string representation of point
 */
function showPoint(p) {
  return `(${p[0]}, ${p[1]})`;
}