const colors = {
  background: [0, 0, 0],
  grid: [255, 255, 100, 0.2],
  oneMeterStep: [0, 255, 0],
  oneResourseSpend: [0, 0, 255],
  startPoint: [255, 0, 0],
  path: {
    left: [255, 0, 255],
    middle: [255, 255, 255],
    right: [255, 255, 0],
  },
};

function drawInfo() {
  ctx.textBaseline = "bottom"; // bottom, middle
  ctx.font = "24px Consolas";
  setFillColor([100, 100, 0]);
  ctx.fillText("0", scene.width / 2 - 20, scene.height / 2);
}

function colorToString(colorVector) {
  if (colorVector.length === 3) {
    const [r, g, b] = colorVector;
    return `rgb(${r}, ${g}, ${b})`;
  } else if (colorVector.length === 4) {
    const [r, g, b, a] = colorVector;
    return `rgba(${r}, ${g}, ${b}, ${a})`;
  }
}

/**
 * @param {number} xFrom @param {number} yFrom @param {number} xTo @param {number} yTo 
 */
function line(xFrom, yFrom, xTo, yTo) {
  ctx.moveTo(xFrom, yFrom);
  ctx.lineTo(xTo, yTo);
}

function setFillColor(color) {
  ctx.fillStyle = colorToString(color);
}

function setStrokeColor(color) {
  ctx.strokeStyle = colorToString(color);
}

function circle(x, y, radius) {
  ctx.arc(x, y, radius, 0, 2 * Math.PI);
}

function drawPath(inputPath, color) {
  push();
  let lastPoint = null;
  setStrokeColor(color);
  setFillColor(colors.oneMeterStep);
  ctx.lineWidth = LINE_WIDTH;
  let path = changePathCoordSystem(inputPath, multiplier, offset);

  for (let i = 0; i < path.length; i++) {
    const point = path[i];
    const [x, y] = point;
    if (i % scene.pathOptions.drawLineEvery === 0) {
      if (lastPoint !== null) {
        const [xlast, ylast] = lastPoint;
        line(xlast, ylast, x, y);
      }
      lastPoint = point;
    }
  }
  ctx.stroke();

  // Accumulated path cost
  let accCost = 0;

  for (let i = 0; i < path.length; i++) {
    const [x, y] = path[i];
    const [cx, cy] = changeCoordSystemBack([x, y], multiplier, offset);
    const cost = n(cx, cy) * scene.pathOptions.step;

    // Draw blue point every 1 resourse spend
    if (accCost + cost >= Math.floor(accCost) + 1) {
      push();

      setFillColor(colors.oneResourseSpend);
      setStrokeColor(colors.oneResourseSpend);
      circle(x, y, scene.circle.radius.blue); // draw a point
      ctx.stroke();
      ctx.fill();

      pop();
    }
    accCost += cost;


    // Draw green point every 1 meter
    if (i % scene.pathOptions.stepInversed === 0) {
      push();

      setStrokeColor(colors.oneMeterStep);
        if (i == 0) {
          setFillColor(colors.startPoint);
          setStrokeColor(colors.startPoint);
        }
      circle(x, y, scene.circle.radius.green); // draw a point
      ctx.stroke();
      ctx.fill();

      pop();
    }
  }
  pop();
}

function drawScene() {
  setFillColor(colors.background);
  ctx.fillRect(0, 0, scene.width, scene.height);
  ctx.fill();
  drawColor((pixelX, pixelY) => {
    let [x, y] = changeCoordSystemBack([pixelX, pixelY], multiplier, offset);
    return (~~(n(x, y) * scene.pixelColorMultiplier + scene.pixelColorOffset)) % 255;
  });
  drawInfo();
  drawGrid(-4, -4, 4, 4, 8, 8, colors.grid);
}

function push() {
  ctx.save();
  ctx.beginPath();
}

function pop() {
  ctx.restore();
}

function drawGrid(x, y, xLast, yLast, nx, ny, color) {
  push();
  setStrokeColor(color);
  x = offset.x + x * multiplier + 0.5;
  y = offset.y + y * multiplier + 0.5;
  xLast = offset.x + xLast * multiplier + 0.5;
  yLast = offset.y + yLast * multiplier + 0.5;
  // draw horizontal lines
  for (let yIndex = 0; yIndex <= ny; yIndex++) {
    let ypos = yIndex * (yLast - y) / ny + y;
    line(x, ypos, xLast, ypos);
  }
  // draw vertical lines
  for (let xIndex = 0; xIndex <= nx; xIndex++) {
    let xpos = xIndex * (xLast - x) / nx + x;
    line(xpos, y, xpos, yLast);
  }
  ctx.stroke();
  pop();
}

/**
 * Draw color function on canvas using `ctx.putImageData(imgData, posX, posY)`
 * @param {(x: number, y: number) => number} colorFunction 
 */
function drawColor(colorFunction) {
  if (cachedImageData !== null) {
    ctx.putImageData(cachedImageData, 0, 0);
    return;
  }
  /** `ImageData { width: w, height: h, data: Uint8ClampedArray(w * h * 4) }` */
  let imgData = ctx.getImageData(0, 0, SCENE_WIDTH, SCENE_HEIGHT);
  let i = 0;
  for (let yi = 0; yi < SCENE_HEIGHT; yi++) {
    for (let xi = 0; xi < SCENE_WIDTH; xi++, i += 4) { 
      const color = colorFunction(xi, yi);
      imgData.data[i  ] = color;
      imgData.data[i+1] = color;
      imgData.data[i+2] = color;
      imgData.data[i+3] = 255;
    }
  }
  ctx.putImageData(imgData, 0, 0);
  cachedImageData = imgData;
}

class ImageLoader {
  constructor(options) {
    this.figuresData = new Uint8ClampedArray();
    this.filename = options.filename || "default.png";
    this.image = new Image();
  }

  static fromFiguresData_default(x, y) {
    const [pixelX, pixelY] = changeCoordSystem([x, y], multiplier, offset);
    const i = (~~pixelY) * SCENE_WIDTH + (~~pixelX);
    // return imageLoader.figuresData[i] / 256;
    return this.figuresData[i] / 256;
  }

  load(afterLoad) {
    this.image.onload = () => {
      const [w, h] = [this.image.width, this.image.height];
      assert(
        w === SCENE_WIDTH && h === SCENE_HEIGHT,
        "image have wrong size"
      );
      ctx.drawImage(this.image, 0, 0);
      const imageData = ctx.getImageData(0, 0, w, h);
      let i = 0;
      this.figuresData = new Uint8ClampedArray(SCENE_HEIGHT * SCENE_WIDTH);
      for (let yi = 0; yi < SCENE_HEIGHT; yi++) {
        for (let xi = 0; xi < SCENE_WIDTH; xi++, i++) {
          this.figuresData[i] = imageData.data[i * 4];
        }
      }
      // fromFiguresData = ImageLoader.fromFiguresData_default;
      fromFiguresData = ImageLoader.fromFiguresData_default.bind(this);
      afterLoad();
    }
  
    this.image.src = `./images/${this.filename}`;
  }
}