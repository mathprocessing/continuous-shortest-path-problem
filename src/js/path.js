function calculateScene(start, finish, alpha, da) {
  const pathLength = scene.pathOptions.length;
  const h = scene.pathOptions.step;
  if (scene.pathOptions.leftRightPathsEnabled) {
    drawPath(calcPath(start, alpha + da, pathLength, h), colors.path.right);
    drawPath(calcPath(start, alpha - da, pathLength, h), colors.path.left);
  }
  let path = calcPath(start, alpha, pathLength, h);
  drawPath(path, colors.path.middle);
  return path;
}

function calcPath(start, startTangent, pathLength, step) {
  const h = step;
  const resultPath = [];
  const [startX, startY] = [start.x, start.y];
  resultPath.push([startX, startY]);
  let [x, y] = [startX, startY];
  let diff = Vector.sub(scene.targetPoint, scene.startPoint);
  let xDirection = diff.x > 0 ? 1 : -1;

  let [normdx, normdy] = normalizeVector(xDirection, xDirection * startTangent, h);
  let currentLength = 0;
  for (let iterations = 0;
    currentLength < pathLength - FLOAT_ERR && iterations < maxIterations; iterations++)
  {
    // | dx dy | = k * n, where k is Curvature of path
    // | gx gy |
    let [gx, gy] = grad(n, x, y, h);

    // -- second method: photon trajectory likeness
    let n_value = n(x, y);
    let [gradlnx, gradlny] = [gx / n_value, gy / n_value];
    //let [gradlnx, gradlny] = [normdy, -normdx];
    // grad(ln(n)) * ds = k, where '*' <=> pseudoScalarProduct

    [normdx, normdy] = normalizeVector(
      normdx + gradlnx * h * h,
      normdy + gradlny * h * h, h);
    // console.log('xy: '+showPoint([x, y])+', norm: '+ showPoint([normdx, normdy]));
    let [dx, dy] = normalizeVector(
      normdx - gradlnx * h * h / 2,
      normdy - gradlny * h * h / 2, h);

    // let [dx, dy] = [
    //   normdx,// - gradlnx * h * h / 2,
    //   normdy// - gradlny * h * h / 2
    // ];

    x += dx; // length of norm = h
    y += dy;
    // console.log('norm: ' + [normdx, normdy]);
    resultPath.push([x, y]);

    currentLength += h;
  }
  return resultPath;
}