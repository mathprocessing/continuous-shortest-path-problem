const KEY = {
  enter: 13,
  backspace: 8,
}

const MB = {
  left: 0,
  middle: 1,
  right: 2,
}

function placeDirectionPoint() {
  setTargetPoint(getMousePos());
}

function placeStartPoint() {
  const mpos = getMousePos();
  const newMouseTarget = {
    x: scene.targetPoint.x + mpos.x - scene.startPoint.x,
    y: scene.targetPoint.y + mpos.y - scene.startPoint.y,
  };
  scene.startPoint = mpos;
  setTargetPoint(newMouseTarget);
}

// Mouse input
cv.addEventListener('mousemove', (e) => {
  [mouseX, mouseY] = [e.pageX, e.pageY];
});

cv.addEventListener('mousedown', (e) => {
  [mouseX, mouseY] = [e.pageX, e.pageY];
  if (e.button === MB.left) {
    placeDirectionPoint();
  } else
    if (e.button === MB.middle) {
      placeStartPoint();
    }
});

// Keyboard input
document.addEventListener('keydown', (e) => {
  if (e.keyCode === KEY.enter) {
    placeDirectionPoint();
  } else
    if (e.keyCode === KEY.backspace) {
      placeStartPoint();
    }
});

// Disable context menu (When user clicked right mouse button)
// window.oncontextmenu = function (e) {
//   e.preventDefault();
// };