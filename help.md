# Load image as ImageData
// https://stackoverflow.com/questions/1445862/possible-to-use-html-images-like-canvas-with-getimagedata-putimagedata
```js
// 1) Create a canvas, either on the page or simply in code
var canvas = document.createElement('canvas');
var ctx = canvas.getContext('2d');

// 2a) Copy your image data into the canvas
var myImgElement = document.getElementById('foo');
ctx.drawImage(myImgElement, 0, 0);

// 2b) Load an image from which to get data
var img = new Image;
img.onload = function () {
  ctx.drawImage(img, 0, 0);
  // ...and then steps 3 and on
};
img.src = "/images/foo.png"; // set this *after* onload

// 3) Read your image data
var w = myImgElement.width, h = myImgElement.height;
var imgdata = ctx.getImageData(0, 0, w, h);
var rgba = imgdata.data;

// 4) Read or manipulate the rgba as you wish
for (var px = 0, ct = w * h * 4; px < ct; px += 4) {
  var r = rgba[px];
  var g = rgba[px + 1];
  var b = rgba[px + 2];
  var a = rgba[px + 3];
}

// 5) Update the context with newly-modified data
ctx.putImageData(imgdata, 0, 0);

// 6) Draw the image data elsewhere, if you wish
someOtherContext.drawImage(ctx.canvas, 0, 0);
```

# Test using n(x, y) = exp(y)
```
startPoint = (0, 0)
length of path = E^2 (E = 2.71828.. = Euler number)
n(x, y) = exp(y) =>
finishPoint: (x, y) = (PI/2, 7.696469...)
```

# Test using equation of circle
```js
*** test circle ***
len = 2 * PI
h = 0.0025, -grad/2 without normalize
0.0018996104556794676, 0.999952138743605

h = 0.0025, +0, without normalize
0.02645122631784130, 0.9917754817401634

h = 0.0025, -grad/2 and normalize enabled
0.0018178539127064507, 0.9999983504624381
***

let v = grad(n, 0.123, 10, 1); // n(x, y) = y * y + x; => grad = (1, 20) = (1, 2y)
let v1 = normalizeVector(3, 4, 1); // (0.6, 0.8)
let v2 = normalizeVector(3, 4, 10); // (6, 8)
print(v1, v2);

h = 0.05
(1.8115289708683004, 3.179208254678293) 
h = 0.1
xy: (1.771355478238998, 3.1018089619344806), norm: (0.032457162439923214, 0.09458611212196237)

h = 0.12
xy: (1.761374498280452, 3.089897731010501), norm: (0.039133587165723085, 0.11343968598132996)
```