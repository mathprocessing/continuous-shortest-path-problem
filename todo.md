* User может сохранить последовательность слайдов (какие пути и между какими вершинами он строил, а также какие функции плотности он использовал)
в `json` файл.
т.е. сохраняется полная история взаимодействия пользователя с программой
+ SaveHistory (automatic)
+ LoadHistory (by user request)

* Create method that plot actual shortest path between `startPoint` and `finishPoint`